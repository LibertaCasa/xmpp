-- Prosody configuration for LibertaCasa

interfaces = { "81.16.19.64", "2a03:4000:47:58a::", "127.0.0.4" }
local_interfaces = { "127.0.0.4" }
http_interface = { "::1" }
https_interface = {}
proxy65_interface = { "81.16.19.64", "2a03:4000:47:58a::" }

http_default_host = "xmpp.liberta.casa"
http_external_url = "https://xmpp.liberta.casa/"
trusted_proxies = { "::1" }

http_upload_external_base_url = "https://up.xmpp.liberta.casa/"
http_upload_external_secret = "$UPSEC"
http_upload_external_file_size_limit = 50000000

push_notification_with_body = true
push_notification_with_sender = true
push_notification_important_body = "LC XMPP Notification"

conversejs_options = {
        debug = true;
        websocket_url = "https://xmpp.liberta.casa/xmpp-websocket";
}

consider_websocket_secure = true
consider_bosh_secure = true
cross_domain_bosh = { "https://xmpp.liberta.casa", "https://liberta.casa" }
---------- Server-wide settings ----------
-- Settings in this section apply to the whole server and are the default settings
-- for any virtual hosts

admins = { "mogad0n@liberta.casa", "acidsys@liberta.casa" }

-- Enable use of libevent for better performance under high load
-- For more information see: https://prosody.im/doc/libevent
use_libevent = true

-- Prosody will always look in its source directory for modules, but
-- this option allows you to specify additional locations where Prosody
-- will look for modules first. For community modules, see https://modules.prosody.im/
plugin_paths = { "/var/lib/prosody/prosody-modules-enabled" }

-- This is the list of modules Prosody will load on startup.
-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
-- Documentation for bundled modules can be found at: https://prosody.im/doc/modules
modules_enabled = {

        -- Generally required
                "roster"; -- Allow users to have a roster. Recommended ;)
                "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
                "tls"; -- Add support for secure TLS on c2s/s2s connections
                "dialback"; -- s2s dialback support
                "disco"; -- Service discovery

        -- Not essential, but recommended
                "carbons"; -- Keep multiple clients in sync
                "pep"; -- Enables users to publish their avatar, mood, activity, playing music and more
                "private"; -- Private XML storage (for room bookmarks, etc.)
                "blocklist"; -- Allow users to block communications with other users
                "vcard4"; -- User profiles (stored in PEP)
                "vcard_legacy"; -- Conversion between legacy vCard and PEP Avatar, vcard

        -- Nice to have
                "version"; -- Replies to server version requests
                "uptime"; -- Report how long server has been running
                "time"; -- Let others know the time here on this server
                "ping"; -- Replies to XMPP pings with pongs
                "register"; -- Allow users to register on this server using a client and change passwords
                "mam"; -- Store messages in an archive and allow users to access it
                "csi_simple"; -- Simple Mobile optimizations

        -- Admin interfaces
                "admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
                "admin_telnet"; -- Opens telnet console interface on localhost port 5582

        -- HTTP modules
                "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
                "websocket"; -- XMPP over WebSockets
                "http_files"; -- Serve static files from a directory over HTTP

        -- Other specific functionality
                --"limits"; -- Enable bandwidth limiting for XMPP connections
                "groups"; -- Shared roster support
                "server_contact_info"; -- Publish contact information for this service
                "announce"; -- Send announcement to all online users
                --"welcome"; -- Welcome users who register accounts
                "watchregistrations"; -- Alert admins of registrations
                "motd"; -- Send a message to users when they log in
                --"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
                -- "proxy65"; -- Enables a file transfer proxy service which clients behind NAT can use # We have this enabled, but defined below as a component

        -- # Manually added modules
                "register_web";
                "admin_adhoc";
                "listusers";
                "list_active";
                "list_inactive";
                "http_auth_check";
                "conversejs";
                --"bookmarks"; --needed for conversejs? #conversejs is apparently only compatible with this legacy bookmarks module, whilst every other client needs bookmarks too. since I care about a bunch of working clients more than about a single weird webclient I will disable this and enable mod_bookmarks2 (below) instead. open for discussion.
                "http_upload_external";
                "cloud_notify";
                --"muc_cloud_notify"; # from my understanding only needed if one wants to run the notif-server on a different domain? if so, would need to be loaded as a component, crashes upon being loaded as a regular module.
                "cloud_notify_encrypted";
                "cloud_notify_filters";
                "cloud_notify_priority_tag";
                --"bookmarks2"; --defective, issue needs to be raised with prosody.
                "seclabels";

}

-- These modules are auto-loaded, but should you want
-- to disable them then uncomment them here:
modules_disabled = {
        -- "offline"; -- Store offline messages
        -- "c2s"; -- Handle client connections
        -- "s2s"; -- Handle server-to-server connections
        -- "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
}

--Custom: Disco Disco: \o/
disco_items = {
        { "biboumi.xyz", "LibertaCasa's public Biboumi instance (IRC Gateway)" }
}

--Custom: Seclabels:
security_labels = { --#disabled the whole thing, as clients were no longer able to connect
  { -- This label will come first
    name = "Public",
    label = true, -- This is a label, but without the actual label.
      default = true -- This is the default label.
  } --if enabling the below, add a comma to this curly bracket
--  {
--    name = "Private",
--    label = "PRIVATE",
--    color = "white",
--    bgcolor = "blue"
--  },
--  Sensitive = { -- A Sub-selector
--    SECRET = { -- The index is used as name
--      label = true
--    },
--    TOPSECRET = { -- The order of this and the above is not guaranteed.
--      color = "red",
--      bgcolor = "black",
--    }
--  }
}

-- Unix specific
pidfile = "/run/prosody/prosody.pid"

-- Disable account creation by default, for security
-- For more information see https://prosody.im/doc/creating_accounts
allow_registration = true

-- Force clients to use encrypted connections? This option will
-- prevent clients from authenticating unless they are using encryption.

c2s_require_encryption = true

-- Force certificate authentication for server-to-server connections?

s2s_secure_auth = true

-- Some servers have invalid or self-signed certificates. You can list
-- remote domains here that will not be required to authenticate using
-- certificates. They will be authenticated using DNS instead, even
-- when s2s_secure_auth is enabled.

--s2s_insecure_domains = { "insecure.example" }

-- Even if you disable s2s_secure_auth, you can still require valid
-- certificates for some domains by specifying a list here.

--s2s_secure_domains = { "jabber.org" }

-- Select the authentication backend to use. The 'internal' providers
-- use Prosody's configured data storage to store the authentication data.

--authentication = "internal_hashed"

-- Select the storage backend to use. By default Prosody uses flat files
-- in its configured data directory, but it also supports more backends
-- through modules. An "sql" backend is included by default, but requires
-- additional dependencies. See https://prosody.im/doc/storage for more info.

storage = "sql" -- Default is "internal"

-- For the "sql" backend, you can uncomment *one* of the below to configure:
--sql = { driver = "SQLite3", database = "prosody.sqlite" } -- Default. 'database' is the filename.
sql = { driver = "MySQL", database = "$DB", username = "$DBUSER", password = "$DBPASS", host = "$DBHOST", port = "3407" }

-- Archiving configuration
-- If mod_mam is enabled, Prosody will store a copy of every message. This
-- is used to synchronize conversations between multiple clients, even if
-- they are offline. This setting controls how long Prosody will keep
-- messages in the archive before removing them.

archive_expires_after = "8w" -- Remove archived messages after 1 week

-- You can also configure messages to be stored in-memory only. For more
-- archiving options, see https://prosody.im/doc/modules/mod_mam

-- Logging configuration
-- For advanced logging see https://prosody.im/doc/logging
log = {
        info = "/var/log/prosody/prosody.log"; -- Change 'info' to 'debug' for verbose logging
        error = "/var/log/prosody/prosody.err";
        -- "*syslog"; -- Uncomment this for logging to syslog
        -- "*console"; -- Log to the console, useful for debugging with daemonize=false
}

-- Uncomment to enable statistics
-- For more info see https://prosody.im/doc/statistics
statistics = "internal"

-- Certificates
-- Every virtual host and component needs a certificate so that clients and
-- servers can securely verify its identity. Prosody will automatically load
-- certificates/keys from the directory specified here.
-- For more information, including how to use 'prosodyctl' to auto-import certificates
-- (from e.g. Let's Encrypt) see https://prosody.im/doc/certificates

-- Location of directory to find certificates in (relative to main config file):
certificates = "certs"

-- HTTPS currently only supports a single certificate, specify it here:
--https_certificate = "/etc/prosody/certs/localhost.crt"

----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

VirtualHost "liberta.casa"
        certificate = "/etc/prosody/certs/liberta.casa.crt"
        http_host = "xmpp.liberta.casa"
        modules_enabled = {
                "admin_web";
        }
VirtualHost "lib.casa"
        certificate = "/etc/prosody/certs/lib.casa.crt"

--VirtualHost "example.com"
--      certificate = "/path/to/example.crt"

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see https://prosody.im/doc/components

---Set up a MUC (multi-user chat) room server on conference.example.com:
Component "conference.liberta.casa" "muc"
--- Store MUC messages in an archive and allow users to access it
modules_enabled = { "muc_mam" }

---Set up an external component (default component port is 5347)
--
-- External components allow adding various services, such as gateways/
-- transports to other networks like ICQ, MSN and Yahoo. For more info
-- see: https://prosody.im/doc/components#adding_an_external_component
--
--Component "gateway.example.com"
--      component_secret = "password"
--
Component "ftproxy.liberta.casa" "proxy65"
        proxy65_address = "ftproxy.liberta.casa"

Component "biboumi.xyz"
        component_secret = "$BB"
--Component "biboumi.liberta.casa"
--      component_secret = "$BB"

